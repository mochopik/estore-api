<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
    * @SWG\Swagger(
    *   @SWG\Info(
    *     title="e-store API",
    *     version="1.0.0",
    *   )
    * ),
    * @SWG\Get(
    *   path="/api/products",
    *   summary="List all product",
    *   @SWG\Response(
    *     response=200,
    *     description="Success "
    *   ),
    *   @SWG\Response(
         response="default",
    *     description="an ""unexpected"" error"
    *   )
    * ),
    * @SWG\Get(
    *   path="/api/products/{id}",
    *   summary="Show product based on ID",
    *   @SWG\Parameter(
    *     name="id",
    *     in="path",
    *     description="Target product ID",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Response(
    *     response=200,
    *     description="Success "
    *   ),
    *   @SWG\Response(
         response="default",
    *     description="an ""unexpected"" error"
    *   )
    * ),
    * @SWG\Post(
    *   path="/api/products",
    *   summary="Creating new product",
    *   @SWG\Parameter(
    *     name="name",
    *     in="formData",
    *     description="Product Name",
    *     required=true,
    *     type="string"
    *   ),
    *   @SWG\Parameter(
    *     name="description",
    *     in="formData",
    *     description="Product Description",
    *     required=true,
    *     type="string"
    *   ),
    *   @SWG\Parameter(
    *     name="price",
    *     in="formData",
    *     description="Product price",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Parameter(
    *     name="stock",
    *     in="formData",
    *     description="Product Stock",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Parameter(
    *     name="discount",
    *     in="formData",
    *     description="Product discount price",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Response(response=200, description="successful operation"),
    *   @SWG\Response(response=406, description="not acceptable"),
    *   @SWG\Response(response=500, description="internal server error")
    * ),
    * @SWG\Get(
    *   path="/api/products/{id}/reviews",
    *   summary="Show product review based on product ID",
    *   @SWG\Parameter(
    *     name="id",
    *     in="path",
    *     description="Target product ID",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Response(
    *     response=200,
    *     description="Success "
    *   ),
    *   @SWG\Response(
         response="default",
    *     description="an ""unexpected"" error"
    *   )
    * ),
    * @SWG\Put(
    *   path="/api/products/{id}",
    *   summary="Update product",
    *   @SWG\Parameter(
    *     name="id",
    *     in="path",
    *     description="Target product ID",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Parameter(
    *     name="name",
    *     in="formData",
    *     description="Product Name",
    *     required=false,
    *     type="string"
    *   ),
    *   @SWG\Parameter(
    *     name="description",
    *     in="formData",
    *     description="Product Description",
    *     required=false,
    *     type="string"
    *   ),
    *   @SWG\Parameter(
    *     name="price",
    *     in="formData",
    *     description="Product price",
    *     required=false,
    *     type="integer"
    *   ),
    *   @SWG\Parameter(
    *     name="stock",
    *     in="formData",
    *     description="Product stock",
    *     required=false,
    *     type="integer"
    *   ),
    *   @SWG\Parameter(
    *     name="discount",
    *     in="formData",
    *     description="Product discount price",
    *     required=false,
    *     type="integer"
    *   ),
    *   @SWG\Response(response=201, description="successful operation"),
    *   @SWG\Response(response=500, description="internal server error")
    * ),
    * @SWG\Delete(
    *   path="/api/products/{id}",
    *   summary="Delete Product",
    *   @SWG\Parameter(
    *     name="id",
    *     in="path",
    *     description="Target product ID",
    *     required=true,
    *     type="integer"
    *   ),
    *   @SWG\Response(
    *     response=204,
    *     description="No Content"
    *   ),
    *   @SWG\Response(
         response="default",
    *     description="an ""unexpected"" error"
    *   )
    * ),
    */
}
